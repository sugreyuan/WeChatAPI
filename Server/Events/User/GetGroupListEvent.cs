﻿using DispatchingCenter.Base;
using Models.User;
using Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Events.User
{
    /// <summary>
    /// 获取所有分组列表事件
    /// </summary>
    public class GetGroupListEvent : DispatchEvent, IAccessTokenAuth
    {
        /// <summary>
        /// 微信接口交互凭证
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 公众平台分组信息列表
        /// </summary>
        public GroupList GroupList { get; set; }
    }
}
