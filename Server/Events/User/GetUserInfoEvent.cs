﻿using DispatchingCenter.Base;
using Models.User;
using Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Events.User
{
    /// <summary>
    /// 获取用户基本信息
    /// </summary>
    public class GetUserInfoEvent : DispatchEvent, IAccessTokenAuth
    {
        /// <summary>
        /// 微信接口交互凭证
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 用户信息查询条件
        /// </summary>
        public UserInfoQueryCondition UserInfoQueryCondition { get; set; }

        /// <summary>
        /// 用户基本信息
        /// </summary>
        public UserInfo UserInfo { get; set; }
    }
}
