﻿using Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.Menu
{
    /// <summary>
    /// 点击推事件菜单
    /// </summary>
    [DataContract(Name = "click_menu")]
    public class ClickMenu : BaseMenu
    {
        /// <summary>
        /// 类型
        /// </summary>
        [DataMember(Name = "type")]
        public string Type { get { return "click"; } set { } }

        /// <summary>
        /// 事件关键字
        /// </summary>
        [DataMember(Name = "key")]
        public string Key { get; set; }
    }
}
