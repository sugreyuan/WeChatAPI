﻿using Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.User
{
    /// <summary>
    /// 关注者列表
    /// </summary>
    [DataContract(Name = "user_list")]
    public class UserList
    {
        /// <summary>
        /// 关注该公众账号的总用户数
        /// </summary>
        [DataMember(Name = "total")]
        public int Total { get; set; }

        /// <summary>
        /// 拉取的OPENID个数，最大值为10000
        /// </summary>
        [DataMember(Name = "count")]
        public int Count { get; set; }

        /// <summary>
        /// 列表数据，OPENID的列表
        /// </summary>
        [DataMember(Name = "data")]
        public OpenIDSet OpenIDSet { get; set; }

        /// <summary>
        /// 拉取列表的后一个用户的OPENID
        /// </summary>
        [DataMember(Name = "next_openid")]
        public string NextOpenID { get; set; }
    }
}
