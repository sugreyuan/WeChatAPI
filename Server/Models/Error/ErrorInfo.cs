﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.Error
{
    /// <summary>
    /// 错误信息
    /// </summary>
    [DataContract(Name = "error")]
    public class ErrorInfo
    {
        /// <summary>
        /// 错误代码
        /// </summary>
        [DataMember(Name = "errcode")]
        public int ErrorCode { get; set; }

        /// <summary>
        /// 错误提示
        /// </summary>
        [DataMember(Name = "errmsg")]
        public string ErrorMessage { get; set; }
    }
}
