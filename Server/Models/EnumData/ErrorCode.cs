﻿using CommonLib.EnumOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.EnumData
{
    /// <summary>
    /// 错误代码
    /// </summary>
    public enum ErrorCode
    {
        /// <summary>
        /// 未知错误
        /// </summary>
        [ExtensionAttribute("未知错误")]
        Unknow = -99999,

        /// <summary>
        /// 系统繁忙
        /// </summary>
        [ExtensionAttribute("系统繁忙")]
        SystemBusy = 1,

        /// <summary>
        /// 请求成功
        /// </summary>
        [ExtensionAttribute("请求成功")]
        Success = 0,

        /// <summary>
        /// 错误的微信签名
        /// </summary>
        [ExtensionAttribute("错误的微信签名")]
        ErrorWeChatSignature = 4000,
    }
}
