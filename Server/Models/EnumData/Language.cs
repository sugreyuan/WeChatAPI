﻿using CommonLib.EnumOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.EnumData
{
    /// <summary>
    /// 语言
    /// </summary>
    public enum Language
    {
        /// <summary>
        /// 简体
        /// </summary>
        [ExtensionAttribute("简体")]
        zh_CN,

        /// <summary>
        /// 繁体
        /// </summary>
        [ExtensionAttribute("繁体")]
        zh_TW,

        /// <summary>
        /// 英语
        /// </summary>
        [ExtensionAttribute("英语")]
        en
    }
}
