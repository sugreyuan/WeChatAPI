﻿using DispatchingCenter.Dispatch;
using Events.SendMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Businesses.SendMessage
{
    /// <summary>
    /// 发送客服消息操作
    /// </summary>
    public class SendCustomerServiceMessageOperation
    {
        /// <summary>
        /// 发送文本消息
        /// </summary>
        /// <param name="customerServiceTextMessage"></param>
        public void SendTextMessage(Models.SendMessage.CustomerServiceTextMessage customerServiceTextMessage)
        {
            var sendCustomerServiceMessageEvent = new SendCustomerServiceMessageEvent()
            {
                CustomerServiceMessage = customerServiceTextMessage
            };

            Dispatcher.ActiveEvent(sendCustomerServiceMessageEvent);
        }
    }
}
