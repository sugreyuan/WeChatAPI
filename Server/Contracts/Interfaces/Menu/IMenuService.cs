﻿using Models.Menu;
using Models.SendMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WCFLib.ExceptionExtension;

namespace Contracts.Interfaces.Menu
{
    [ServiceContract]
    public interface IMenuService
    {
        [OperationContract]
        [FaultContractAttribute(typeof(FaultMessage))]
        void CreateMenu(MenuList menuList);

        [OperationContract]
        [FaultContractAttribute(typeof(FaultMessage))]
        MenuList GetMenu();
    }
}
