﻿using Contracts.Interfaces.Auth;
using Contracts.Interfaces.User;
using Contracts.Interfaces.ReceiveMessage;
using Contracts.Interfaces.SendMessage;
using Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFLib.ExceptionExtension;
using Contracts.Interfaces.Menu;

namespace Contracts.Services
{
    [GlobalExceptionHandlerBehaviour(typeof(GlobalExceptionHandler))]
    [System.ServiceModel.ServiceBehavior(ConcurrencyMode = System.ServiceModel.ConcurrencyMode.Multiple)]
    public class WeChatService :
        IAuthService,
        IGroupService,
        IReceiveMessageService,
        ISendCustomerServiceMessageService,
        IUserService,
        IMenuService
    {
        #region IAuth 成员

        public void Auth(AuthInfo authInfo)
        {
            new Businesses.Auth.AuthOperation().Auth(authInfo);
        }

        public string GetOpenIDByOAuthCode(string code)
        {
            return new Businesses.Auth.AuthOperation().GetOpenIDByOAuthCode(code);
        }

        #endregion

        #region IGroupService 成员

        public Models.User.GroupList GetGroupList()
        {
            return new Businesses.User.UserOperation().GetGroupList();
        }

        #endregion

        #region IReceiveMessageService 成员

        public void ReceiveRawMessage(string rawMessage)
        {
            Console.WriteLine(rawMessage);
            System.Text.RegularExpressions.Regex regContent = new System.Text.RegularExpressions.Regex(@"\<Content\>.*\</Content\>");
            var match = regContent.Match(rawMessage);
            if (match.Success)
            {
                Console.WriteLine(match.Value);
            }
        }

        #endregion

        #region ISendCustomerServiceMessageService 成员

        public void SendCustomerServiceTextMessage(Models.SendMessage.CustomerServiceTextMessage customerServiceTextMessage)
        {
            new Businesses.SendMessage.SendCustomerServiceMessageOperation().SendTextMessage(customerServiceTextMessage);
        }

        #endregion

        #region IUserService 成员

        public Models.User.UserList GetUserList(Models.User.UserList userList)
        {
            return new Businesses.User.UserOperation().GetUserList(userList);
        }

        public Models.User.UserInfo GetUserInfo(Models.User.UserInfoQueryCondition userInfoQueryCondition)
        {
            return new Businesses.User.UserOperation().GetUserInfo(userInfoQueryCondition);
        }

        #endregion

        #region IMenuService 成员

        public void CreateMenu(Models.Menu.MenuList menuList)
        {
            new Businesses.Menu.MenuOperation().CreateMenu(menuList);
        }

        public Models.Menu.MenuList GetMenu()
        {
            return new Businesses.Menu.MenuOperation().GetMenu();
        }

        #endregion
    }
}
