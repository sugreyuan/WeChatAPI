﻿using Commands.Base;
using DispatchingCenter.Base;
using Events.SendMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commands.SendMessage
{
    /// <summary>
    /// 发送客服消息命令
    /// </summary>
    public class SendCustomerServiceMessageCommand
    {
        [DispatchHandler(typeof(SendCustomerServiceMessageEvent))]
        public void SendCustomerServiceMessage(SendCustomerServiceMessageEvent e)
        {
            var commandRequest = new SendCustomerServiceMessageCommandRequest(e.AccessToken, e.CustomerServiceMessage);

            CommandHelper.NoReturnWeChatRequest(commandRequest);
        }
    }
}
