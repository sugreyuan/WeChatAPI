﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DispatchingCenter.Base
{
    /// <summary>
    /// 调度处理器
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class DispatchHandlerAttribute : Attribute
    {
        /// <summary>
        /// 观察的调度事件类型
        /// </summary>
        public Type ObservedDispatchEventType { get; set; }

        /// <summary>
        /// 调度实例
        /// </summary>
        public object DispatchInstance { get; set; }

        /// <summary>
        /// 动作方法信息
        /// </summary>
        public MethodInfo ActionMethodInfo { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="observedDispatchEventType">观察的调度事件类型</param>
        public DispatchHandlerAttribute(Type observedDispatchEventType)
        {
            this.ObservedDispatchEventType = observedDispatchEventType;
        }
    }
}
